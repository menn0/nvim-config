return {
  'echasnovski/mini.operators',
  opts = {
    replace = {
      prefix = 'S',

      -- Whether to reindent new text to match previous indent
      reindent_linewise = true,
    },
    -- gx is "exchange"
  },
}
