function toggle_inlay_hints()
  vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled(), nil)
end

function config()
  local lspconfig = require('lspconfig')

  -- This requires a patched font (e.g. one of the nerd fonts)
  local signs = { Error = "", Warn = "", Hint = "", Info = " " }
  for type, icon in pairs(signs) do
    local hl = "DiagnosticSign" .. type
    vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
  end

  vim.diagnostic.config({
    virtual_text = false,
    severity_sort = true,
    signs = true,
  })

  -- on_attach is called whenever a buffer is attached an LSP server
  local on_attach = function(client, bufnr)
    -- Let conform handle formatting
    client.server_capabilities.documentFormattingProvider = false

    local function set_opt(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    -- Mappings
    local opts = { silent=true, buffer=bufnr }
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gK', vim.diagnostic.open_float, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set({'n', 'i'}, '<F1>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', ',R', vim.lsp.buf.rename, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<leader>i', toggle_inlay_hints, opts)
    vim.keymap.set('n', 'gp', function()
        vim.diagnostic.goto_prev({float = false})
    end, opts)
    vim.keymap.set('n', 'gn', function()
        vim.diagnostic.goto_next({float = false})
    end, opts)

    vim.wo.signcolumn = 'yes'
  end

  lspconfig.gopls.setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    },
    settings = {
      gopls = {
        hints = {
          rangeVariableTypes = true,
          parameterNames = true,
          constantValues = true,
          assignVariableTypes = true,
          compositeLiteralFields = true,
          compositeLiteralTypes = true,
          functionTypeParameters = true,
        },
      }
    }
  }

  lspconfig.ts_ls.setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    },
  }

  lspconfig.basedpyright.setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    },
  }
  -- Useful pyrightconfig.json to go with the above
  -- {
  --     "venvPath": ".",
  --     "venv": ".venv",
  --     "typeCheckingMode": "standard",
  --     "enableReachabilityAnalysis": false,
  --     "reportUnreachable": false
  -- }

  -- Rust config is a little different because we want rust_analyzer to run
  -- clippy on save.
  lspconfig.rust_analyzer.setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    },
    settings = {
      ["rust-analyzer"] = {
        checkOnSave = true,
        check = {
          enable = true,
          command = "clippy",
          features = "all",
        },
        inlayHints = {
          bindingModeHints = {
            enable = false,
          },
          chainingHints = {
            enable = true,
          },
          closingBraceHints = {
            enable = true,
            minLines = 25,
          },
          closureReturnTypeHints = {
            enable = "never",
          },
          lifetimeElisionHints = {
            enable = "never",
            useParameterNames = false,
          },
          maxLength = 25,
          parameterHints = {
            enable = true,
          },
          reborrowHints = {
            enable = "never",
          },
          renderColons = true,
          typeHints = {
            enable = true,
            hideClosureInitialization = false,
            hideNamedConstructor = false,
          },
        },
      },
    },
  }
end

return {
  "neovim/nvim-lspconfig",
  config = config,
}
