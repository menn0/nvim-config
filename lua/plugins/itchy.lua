return {
  "idbrii/itchy.vim",
  init = function() 
    vim.cmd("let g:itchy_always_split=1")
  end,
  keys = {
    {'<Leader>z', '<Plug>(itchy-open-scratch)'},
  },
}
