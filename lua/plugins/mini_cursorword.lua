return {
  'echasnovski/mini.cursorword',
  lazy = false,
  config = function()
    require('mini.cursorword').setup()

    vim.api.nvim_set_hl(0, 'MiniCursorword', { bg='#1d3b53' })
    vim.api.nvim_set_hl(0, 'MiniCursorwordCurrent', { guifg=nil, guibg=nil, gui=nil, cterm=nil })
  end,
}
