return {
  "epwalsh/obsidian.nvim",
  version = "*",  -- recommended, use latest release instead of latest commit
  lazy = true,
  ft = "markdown",
  cmd = "ObsidianQuickSwitch",
  dependencies = {
    'nvim-lua/plenary.nvim',
    'hrsh7th/nvim-cmp',
  },
  opts = {
    workspaces = {
      {
        name = "Main",
        path = "~/Obsidian/Main",
      },
    },
    completion = {
      nvim_cmp = true,
      min_chars = 1,
    },
    daily_notes = {
      default_tags = { },
      template = nil
    },
    ui = {
     checkboxes = {
       [" "] = { char = "󰄱", hl_group = "ObsidianTodo" },
       ["/"] = { char = "/", hl_group = "ObsidianTodo" },
       ["x"] = { char = "x", hl_group = "ObsidianDone" },
     },
    },
    note_frontmatter_func = function(note)
      return note.metadata
    end,
    follow_url_func = function(url)
      vim.ui.open(url)
    end,
  },
  keys = {
    {'<leader>oo', ':ObsidianQuickSwitch<cr>', mode="n"},
    {'<leader>ot', ':ObsidianToday<cr>', mode="n"},
  {'<a-x>', function()
        return require("obsidian").util.toggle_checkbox({" ", "/", "x"})
      end, mode={"n", "i"},
    },
  }
}
