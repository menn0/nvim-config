return {
  "echasnovski/mini.move",
  opts = true,
  keys = {
    {"<M-h>", mode={"v", "n"}},
    {"<M-l>", mode={"v", "n"}},
    {"<M-j>", mode={"v", "n"}},
    {"<M-k>", mode={"v", "n"}},
  },
}
