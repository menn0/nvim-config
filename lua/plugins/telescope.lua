function setup()
  local builtin = require("telescope.builtin")
  local utils = require("telescope.utils")

  require("telescope").setup {
     defaults = vim.tbl_extend("force",
      require('telescope.themes').get_ivy(),
      {
        file_ignore_patterns = { "^.git/" },
      }
    ),

    pickers = {
      buffers = {
        sort_lastused = true,
        ignore_current_buffer = true,
        previewer = false,
      },
      find_files = {
        previewer = false,
        hidden = true,
      },
      oldfiles = {
        previewer = false,
      },
    },

    extensions = {
      file_browser = {
        theme  = "ivy",
      }
    }
  }

  function find_in_file_dir()
    builtin.find_files { cwd = utils.buffer_dir(), }
  end

  -- Find files but don't respect .gitignore etc
  function find_files_no_ignore()
    builtin.find_files { no_ignore = true }
  end

  function find_in_config()
    builtin.find_files { cwd = vim.fn.stdpath('config') }
  end

  function live_grep_in_file_dir()
    require('telescope').extensions.live_grep_args.live_grep_args({
      cwd = utils.buffer_dir()
    })
  end

  function buffers_sorted_by_mru()
      builtin.buffers({sort_mru=true})
  end

  vim.keymap.set('n', '<leader>f', builtin.find_files)
  vim.keymap.set('n', '<leader>F', builtin.oldfiles)
  vim.keymap.set('n', '<leader>I', find_files_no_ignore)
  vim.keymap.set('n', '<leader>b', buffers_sorted_by_mru)
  vim.keymap.set("n", "<leader>r", require('telescope').extensions.live_grep_args.live_grep_args)
  vim.keymap.set("n", "<leader>R", builtin.resume)
  vim.keymap.set('n', '<leader>D', live_grep_in_file_dir)
  vim.keymap.set('n', '<leader>.', builtin.grep_string)
  vim.keymap.set('n', '<leader>B', builtin.current_buffer_fuzzy_find)
  vim.keymap.set('n', '<leader>d', find_in_file_dir)
  vim.keymap.set('n', '<leader>x', ":Telescope file_browser<CR>")
  vim.keymap.set('n', '<leader>X', ":Telescope file_browser path=%:p:h select_buffer=true<CR>")
  vim.keymap.set('n', '<leader>c', find_in_config)
  vim.keymap.set('n', '<leader>:', builtin.command_history)
  vim.keymap.set({'n', 'i'}, '<A-s>', builtin.spell_suggest)
end

return {
  {
    'nvim-telescope/telescope.nvim',
     dependencies = { 'nvim-lua/plenary.nvim' },
     config = setup,
  },
  {
    'nvim-telescope/telescope-fzy-native.nvim',
    config = function()
      require('telescope').load_extension('fzy_native')
    end
  },
  {
    'nvim-telescope/telescope-live-grep-args.nvim',
    config = function()
      require('telescope').load_extension('live_grep_args')
    end
  },
  {
    "nvim-telescope/telescope-file-browser.nvim",
    dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
    config = function()
      require('telescope').load_extension('file_browser')
    end
  }
}
