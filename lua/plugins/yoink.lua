 -- yank/paste ring
return {
  "svermeulen/vim-yoink",
  config = function()
    vim.cmd([[
      " Allow continuous cycling
      let g:yoinkSwapClampAtEnds = 0

      let g:yoinkIncludeDeleteOperations = 1
    ]])
  end,
  keys = {

    {'<a-p>', '<plug>(YoinkPostPasteSwapBack)'},
    {'<a-s-p>', '<plug>(YoinkPostPasteSwapForward)'},

    {'p', '<plug>(YoinkPaste_p)'},
    {'P', '<plug>(YoinkPaste_P)'},

    -- Also replace the default gp with yoink paste so we can toggle paste in this case too
    {'gp', '<plug>(YoinkPaste_gp)'},
    {'gP', '<plug>(YoinkPaste_gP)'},
  },
}
