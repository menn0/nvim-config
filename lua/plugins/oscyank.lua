-- Reliable system clipboard iteraction
return {
  'ojroques/vim-oscyank',
  keys = {
    {'<leader>y', '<Plug>OSCYankVisual', mode="v"},
    {'<leader>y', '<Plug>OSCYankOperator', mode="n"},
    {'<leader>Y', '<leader>y_', mode="n"},
  },
}
