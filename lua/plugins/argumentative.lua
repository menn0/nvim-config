return {
  "PeterRincker/vim-argumentative",
  keys = {
    {'gh', '<Plug>Argumentative_Prev', mode='n'},
    {'gl', '<Plug>Argumentative_Next', mode='n'},
    {'gh', '<Plug>Argumentative_XPrev', mode='x'},
    {'gl', '<Plug>Argumentative_XNext', mode='x'},

    {'gH', '<Plug>Argumentative_MoveLeft', mode='n'},
    {'gL', '<Plug>Argumentative_MoveRight', mode='n'},

    {'ia', '<Plug>Argumentative_InnerTextObject', mode='x'},
    {'aa', '<Plug>Argumentative_OuterTextObject', mode='x'},
    {'ia', '<Plug>Argumentative_OpPendingInnerTextObject', mode='o'},
    {'aa', '<Plug>Argumentative_OpPendingOuterTextObject', mode='o'},
  },
}
