return {
  "tpope/vim-fugitive",
  cmd = { "Git", "G" },
  keys = {
    { "<leader>g", "<cmd>:Git<CR>" },
  }
}
