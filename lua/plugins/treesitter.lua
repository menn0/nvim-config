return {
  {
    'nvim-treesitter/nvim-treesitter',
    build = ":TSUpdate",
    config = function()
      local configs = require("nvim-treesitter.configs")

      configs.setup({
        ensure_installed = {
          "lua",
          "vim",
          "vimdoc",
          "rust",
          "go",
          "python",
          "javascript",
          "typescript",
          "tsx",
          "json",
          "yaml",
          "markdown",
          "markdown_inline",
        },

        highlight = {
          enable = true,
          additional_vim_regex_highlighting = false,
          disable = { "vim" },
        },

        incremental_selection = {
          enable = true,
          disable = { "vim" },
          keymaps = {
            init_selection = "<cr>",
            scope_incremental = "<cr>",
            node_incremental = "<space>",
            node_decremental = "<backspace>",
          },
        },
      })
    end
  },
  {
     'nvim-treesitter/nvim-treesitter-textobjects'
  },
}
