return {
  "mitsuhiko/vim-jinja",
  init = function() 
    vim.cmd([[
      autocmd BufRead,BufNewFile *.jinja set filetype=jinja
      autocmd BufRead,BufNewFile *.sls set filetype=jinja

      let g:htmljinja_disable_detection=1 
    ]])
  end,
  ft = "jinja",
}
