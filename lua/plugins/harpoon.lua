return {
  "ThePrimeagen/harpoon",
  config = function() 
    local mark = require("harpoon.mark")
    local ui = require("harpoon.ui")

    vim.keymap.set("n", "<leader>a", mark.add_file)
    vim.keymap.set("n", "<C-e>", ui.toggle_quick_menu)
    vim.keymap.set("n", "<C-S-e>", mark.clear_all)

    for i=1,6 do
      vim.keymap.set("n", string.format("<leader>%d", i), function() ui.nav_file(i) end)
    end
  end,
  keys = {
    '<leader>a',
    '<C-e>',
    '<C-S-e>',
    '<leader>1',
    '<leader>2',
    '<leader>3',
    '<leader>4',
    '<leader>5',
    '<leader>6',
  },
}
