return {
  'echasnovski/mini.bufremove',
  config = function() 
    require('mini.bufremove').setup()

    vim.api.nvim_create_user_command(
      'BD',
      function () MiniBufremove.delete(0) end,
      { desc = "Delete buffer but preserve window layout" }
    )
  end,
  cmd = "BD",
}
