-- Fancy 2d jump
return {
  'folke/flash.nvim',
  config = function()
    local flash = require("flash")
    vim.keymap.set({'n', 'x', 'o'}, '<C-j>', flash.jump)
    vim.keymap.set({'n', 'x', 'o'}, '<C-k>', flash.treesitter)
  end,
  keys = {
    {"<C-j>", mode={'n', 'x', 'o'}},
    {"<C-k>", mode={'n', 'x', 'o'}},
  },
}
