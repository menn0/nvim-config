-- TODO: convert me
vim.cmd([[
set background=dark
set termguicolors

highlight ExtraWhitespace guibg='#1d3b53'
match ExtraWhitespace /\s\+\%#\@<!$/

set cursorline      " highlight current line
set title           " set the terminal title

" Set cursor blinking and shapes for different modes
set guicursor=a:blinkwait1-blinkoff300-blinkon300-Cursor/lCursor,n-v-c-sm:block,i-ci-ve:ver25,r-cr:hor20,o:hor50

" rebalance windows if console is resize
autocmd VimResized * :wincmd =

" Show more context when scrolling near the edge of a window
set scrolloff=8

" Stay centered while scrolling
nmap <C-d> <C-d>zz
nmap <C-u> <C-u>zz

" Keep buffers equal
set equalalways
]])
