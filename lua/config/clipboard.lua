-- Paste from clipboard
vim.keymap.set("n", "<leader>p", '"+p')
vim.keymap.set("n", "<leader>P", '"+P')
vim.keymap.set("v", "<leader>p", '"+p')
vim.keymap.set("v", "<leader>P", '"+P')

vim.keymap.set("n", "<A-i>", "<cmd>set invpaste<CR>")

-- Copy path to clipboard
-- Relative
vim.keymap.set("n", "<leader>0", '<cmd>let @+ = expand("%")<CR>')
-- Full
vim.keymap.set("n", "<leader>9", '<cmd>:let @+ = expand("%:p")<CR>')
