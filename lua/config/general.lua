-- TODO: convert me
vim.cmd([[
set nocompatible
set expandtab
set ts=4
set sts=4
set shiftwidth=4
set incsearch
set ignorecase
set smartcase
set hidden
set wildmode=longest,list
filetype indent plugin on

nnoremap <C-n> :cn<CR>
nnoremap <C-p> :cp<CR>

" A quick way to turn off search highlighting
nnoremap <leader>, :nohlsearch<CR>

set updatetime=500

set spell

" Move by wrapped lines, not physical lines
nnoremap j gj
nnoremap k gk
nnoremap 0 g0
nnoremap $ g$
nnoremap ^ g^

" Undo setup
let undo_path = expand('~/.cache/nvim-undo')
if !isdirectory(undo_path)
    call mkdir(undo_path, "p", 0700)
endif
let &undodir=undo_path
set undofile

" Open file in same directory as current buffer.
nnoremap <leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

" Change pwd to directory of current file
nnoremap <leader>` :chdir <C-R>=expand("%:p:h")<CR><CR>

" Insert current ISO date
inoremap <A-d> <C-R>=strftime("%Y-%m-%d")<CR>

" Interactive search and replace word at cursor
nnoremap <leader>S :%s/<C-r><C-w>//g<Left><Left>

" Trim trailing whitespace
nnoremap <leader>; :silent! %s/\s\+$//<CR>:silent nohlsearch<CR>

" Move visual selection up and down
vmap J :m '>+1<CR>gv=gv
vmap K :m '<-2<CR>gv=gv

" Handle for filtering quicklist
packadd cfilter

let g:netrw_browse_split=0

" Stop suspend in Windows because it doesn't work
if has('win32') 
    noremap <C-z> <nop>
endif
]])
