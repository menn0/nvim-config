- gc* doesn't always work 
  * required a fairly new neovim
  * tries treesitter
  * falls back to commmentstring
- Lazy -  https://github.com/folke/lazy.nvim
- Check out live subst:  
https://www.reddit.com/r/neovim/comments/r77piz/live_substitution_by_default_on_neovim_06_thanks/
- Try out autosave:  https://github.com/907th/vim-auto-save
- cleaner LSP tab insertion
- longer leader timeout 
- require enter less often on command output
